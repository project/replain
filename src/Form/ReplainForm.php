<?php

namespace Drupal\replain\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * ReplainForm class for implementing replain_form.settings forms.
 */
class ReplainForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'replain.form_settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'replain_form.settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['replain.form_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config(static::SETTINGS);

    $form['helper'] = [
      '#type' => 'item',
      '#markup' => '<p>' . $this->t('To connect Re:plain you need to get the Re:plain code. To do this, follow the link below.') . '</p><p><a class="replain-bot-link" href="https://replain.cc/" target="_blank">' . $this->t('Get the Re:plain code') . '</a></p>',
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Re:plain status'),
      '#default_value' => $config->get('status'),
    ];

    $form['code'] = [
      '#title'         => $this->t('Enter your Re:plain code'),
      '#type'          => 'textarea',
      '#default_value' => $config->get('code'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->getEditable(static::SETTINGS);
    $code = $form_state->getValue('code');
    $token = $config->get('code');
    \preg_match('/REPLAIN_\s*=\s*\W(.*?)\W;/', \stripslashes($code), $matches);

    if (!empty($matches) && isset($matches[1])) {
      $token = $matches[1];
    }
    else {
      \preg_match('/id\s*:\s*\W(.*?)\W{2}/', \stripslashes($code), $matches);
      if (!empty($matches) && isset($matches[1])) {
        $token = $matches[1];
      }
    }

    $config
      ->set('status', $form_state->getValue('status'))
      ->set('code', $token);

    $config->save();
    parent::submitForm($form, $form_state);
  }

}
