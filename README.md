CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Re:plain – the first and the simplest live chat in your messenger.
Messages from your website comes directly to your Facebook
Messenger or Telegram and back.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   Visit [Installing Modules](https://www.drupal.org/node/1897420)
   for further information.


CONFIGURATION
-------------

    1 Click on "Re:plain" in the menu and follow the instruction on the page.


MAINTAINERS
-----------

Current maintainers:

 * UsingSession (https://www.drupal.org/u/usingsession)
